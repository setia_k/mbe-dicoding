const { Book, bookshelf } = require('../models/books');

const BookHandlerPost = (req, head) => {
  const { payload } = req;
  let response;
  let newBook;
  let errorFlag = 0;

  if (!payload.name || payload.name.trim() === '') {
    errorFlag = 1;
  } else if (payload.readPage > payload.pageCount) {
    errorFlag = 2;
  }

  if (errorFlag === 0) {
    try {
      newBook = new Book(payload);
      bookshelf.set(newBook.id, newBook);
    } catch (error) {
      errorFlag = 3;
    }
  }

  switch (errorFlag) {
    // SUCCESS
    case 0:
      response = head.response({
        status: 'success',
        message: 'Buku berhasil ditambahkan',
        data: {
          bookId: newBook.id,
        },
      });
      response.code(201);
      break;
    // NO NAME
    case 1:
      response = head.response({
        status: 'fail',
        message: 'Gagal menambahkan buku. Mohon isi nama buku',
      });
      response.code(400);
      break;
    // READ LARGER THAN PAGE
    case 2:
      response = head.response({
        status: 'fail',
        message: 'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount',
      });
      response.code(400);
      break;
    // OTHERS
    default:
      response = head.response({
        status: 'error',
        message: 'Buku gagal ditambahkan',
      });
      response.code(500);
      break;
  }
  return response;
};

module.exports = BookHandlerPost;
