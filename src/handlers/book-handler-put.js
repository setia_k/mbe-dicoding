const { bookshelf } = require('../models/books');

const BookHandlerPut = (req, head) => {
  const { payload } = req;
  const { bookId } = req.params;
  const searchedBook = bookshelf.get(bookId);
  let response;
  let errorFlag = 0;

  if (!payload.name || payload.name.trim() === '') {
    errorFlag = 1;
  } else if (payload.readPage > payload.pageCount) {
    errorFlag = 2;
  }

  if (errorFlag === 0) {
    try {
      if (searchedBook) {
        searchedBook.updateData(payload);
      } else {
        errorFlag = 3;
      }
    } catch (error) {
      errorFlag = 4;
    }
  }

  switch (errorFlag) {
    // SUCCESS
    case 0:
      response = head.response({
        status: 'success',
        message: 'Buku berhasil diperbarui',
      });
      response.code(200);
      break;
    // NO NAME
    case 1:
      response = head.response({
        status: 'fail',
        message: 'Gagal memperbarui buku. Mohon isi nama buku',
      });
      response.code(400);
      break;
    // READ LARGER THAN PAGE
    case 2:
      response = head.response({
        status: 'fail',
        message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount',
      });
      response.code(400);
      break;
    // ID NOT FOUND
    case 3:
      response = head.response({
        status: 'fail',
        message: 'Gagal memperbarui buku. Id tidak ditemukan',
      });
      response.code(404);
      break;
    // OTHERS
    default:
      response = head.response({
        status: 'error',
        message: 'Buku gagal diperbarui',
      });
      response.code(500);
      break;
  }

  return response;
};

module.exports = BookHandlerPut;
