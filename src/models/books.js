const { nanoid } = require('nanoid');

class Book {
  constructor(content) {
    this.id = nanoid(8);
    this.name = content.name;
    this.year = content.year;
    this.author = content.author;
    this.summary = content.summary;
    this.publisher = content.publisher;
    this.pageCount = content.pageCount;
    this.readPage = content.readPage;
    this.finished = content.readPage === content.pageCount;
    this.reading = content.reading;
    this.insertedAt = new Date().toISOString();
    this.updatedAt = this.insertedAt;
  }

  updateData(content) {
    this.name = content.name;
    this.year = content.year;
    this.author = content.author;
    this.summary = content.summary;
    this.publisher = content.publisher;
    this.pageCount = content.pageCount;
    this.readPage = content.readPage;
    this.reading = content.reading;
    this.updatedAt = new Date().toISOString();
  }
}

const bookshelf = new Map();

module.exports = { Book, bookshelf };
