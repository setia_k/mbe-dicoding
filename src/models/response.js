const responseSuccess = () => null;
const responseError = () => null;
const responseFail = () => null;

module.exports = { responseSuccess, responseError, responseFail };
